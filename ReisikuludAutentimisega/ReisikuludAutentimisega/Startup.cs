﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ReisikuludAutentimisega.Startup))]
namespace ReisikuludAutentimisega
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app); 
        }
    }
}
