﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ReisikuludAutentimisega.Models;
using System.IO;

namespace ReisikuludAutentimisega.Controllers
{
    public class SpendingsController : Controller
    {
        private WienerEntities1 db = new WienerEntities1();



        //public ActionResult File(HttpPostedFileBase file)
        //{
        //    if (file.ContentLength > 0)
        //        try
        //        {
        //            file.SaveAs(Path.Combine(Server.MapPath("~/..."), Path.GetFileName(file.FileName)));
        //            return File(file, "image/jpg");

        //        }

        //        catch
        //        {
        //            ViewBag.Message = "Faili ei lisatud";
        //        }



        //}

        private ActionResult File(HttpPostedFileBase file, string v)
        {
            throw new NotImplementedException();
        }



        // GET: Spendings
        public ActionResult Index()
        {
            var spendings = db.Spendings.Include(s => s.File).Include(s => s.Travel).Include(s => s.SpendingType);
            return View(spendings.ToList());
        }

        // GET: Spendings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Spending spending = db.Spendings.Find(id);
            if (spending == null)
            {
                return HttpNotFound();
            }
            return View(spending);
        }

        // GET: Spendings/Create
        public ActionResult Create(int TravelID)
        {


            ViewBag.Document = new SelectList(db.Files, "Id", "Filename");
            ViewBag.TravelId = new SelectList(db.Travels, "Id", "Id");
            ViewBag.TypeId = new SelectList(db.SpendingTypes, "Id", "SpendingName");
            
            return View(new Spending { TravelId = TravelID });
        }

        // POST: Spendings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,TravelId,TypeId,Amount,Document")] Spending spending, HttpPostedFileBase file, ReportLog report)
        {
            if (ModelState.IsValid)
            {
                db.Spendings.Add(spending);                
                db.SaveChanges();
                int id1 = spending.Id;
                ReportLog.Log(report, $"New spending added: {id1}", Person.GetByEmail(User.Identity.Name).Id);
                if (file != null && file.ContentLength > 0)
                {
                    using (BinaryReader br = new BinaryReader(file.InputStream))
                    {
                        byte[] buff = br.ReadBytes(file.ContentLength);
                        Models.File mFile = new Models.File
                        {
                            Content = buff,
                            ContentType = file.ContentType,
                            Filename = file.FileName.Split('\\').LastOrDefault()??"no name"
                        };
                        db.Files.Add(mFile);
                        db.SaveChanges();

                        spending.Document = mFile.Id;
                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Edit", "Travels", new { id = spending.TravelId });
            }

            ViewBag.Document = new SelectList(db.Files, "Id", "Filename", spending.Document);
            ViewBag.TravelId = new SelectList(db.Travels, "Id", "TravelName", spending.TravelId);
            ViewBag.TypeId = new SelectList(db.SpendingTypes, "Id", "SpendingName", spending.TypeId);
            return View(spending);
        }
        

        // GET: Spendings/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Spending spending = db.Spendings.Find(id);
            if (spending == null)
            {
                return HttpNotFound();
            }
            ViewBag.Document = new SelectList(db.Files, "Id", "Filename", spending.Document);
            ViewBag.TravelId = new SelectList(db.Travels, "Id", "TravelName", spending.TravelId);
            ViewBag.TypeId = new SelectList(db.SpendingTypes, "Id", "SpendingName", spending.TypeId);
            return View(spending);
        }

        // POST: Spendings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,TravelId,TypeId,Amount,Document")] Spending spending, ReportLog report)
        {
            if (ModelState.IsValid)
            {
                db.Entry(spending).State = EntityState.Modified;
                db.SaveChanges();
                int id1 = spending.Id;
                ReportLog.Log(report, $"Spending edited: {id1}", Person.GetByEmail(User.Identity.Name).Id);
                return RedirectToAction("Edit", "Travels", new { id = spending.TravelId });
            }
            ViewBag.Document = new SelectList(db.Files, "Id", "Filename", spending.Document);
            ViewBag.TravelId = new SelectList(db.Travels, "Id", "TravelName", spending.TravelId);
            ViewBag.TypeId = new SelectList(db.SpendingTypes, "Id", "SpendingName", spending.TypeId);
            return View(spending);
        }

        // GET: Spendings/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Spending spending = db.Spendings.Find(id);
            if (spending == null)
            {
                return HttpNotFound();
            }
            return View(spending);
        }

        // POST: Spendings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, ReportLog report)
        {
            Spending spending = db.Spendings.Find(id);
            db.Spendings.Remove(spending);
            db.SaveChanges();
            int id1 = spending.Id;
            ReportLog.Log(report, $"Spending deleted: {id1}", Person.GetByEmail(User.Identity.Name).Id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
