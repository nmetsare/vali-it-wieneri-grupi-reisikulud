﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ReisikuludAutentimisega.Models;
using System.Linq.Expressions;
using PagedList.Mvc;
using PagedList;

namespace ReisikuludAutentimisega.Models
{
    partial class Travel
    {
        static WienerEntities1 db = new WienerEntities1();

        public int? Duration => StartDate.HasValue && EndDate.HasValue ? (EndDate.Value - StartDate.Value).Days + 1 : 0;
        public int? Nights => StartDate.HasValue && EndDate.HasValue ? (EndDate.Value - StartDate.Value).Days : 0;
        public int? Päevarahad => ((EndDate.Value - StartDate.Value).Days + 1) * 50;

        public static Dictionary<int, string> States = new Dictionary<int, string>
        {
            {0, "Koostamisel" },
            {1, "Esitatud" },
            {2, "Tagasi lükatud" },
            {3, "Kinnitatud" },
            {4, "Välja makstud" },
        };

        public string State => States[this.ReportState];     

        public static Spending GetSpending(int SpendID)
            => db.Spendings.Where(x => x.TravelId == SpendID).SingleOrDefault();


    }       

}

namespace ReisikuludAutentimisega.Controllers
{
    public class TravelsController : Controller
    {
        private WienerEntities1 db = new WienerEntities1();

        public object Days { get; private set; }

        // GET: Travels
        public ActionResult Index(string search,  int? i, string otsing, string sort = "StartDate", string direction = "ascending", string fStart = "", string fEnd = "", int no = 10)
        {

            var travels = db.Travels.Include(t => t.Person);

            ViewBag.Sort = sort;
            ViewBag.Direction = direction;

            var sorted =
              (direction == "descending" && sort == "StartDate") ? travels.OrderByDescending(x => x.StartDate)
              : (sort == "StartDate") ? travels.OrderBy(x => x.StartDate)
              : (direction == "descending" && sort == "ReportState") ? travels.OrderByDescending(x => x.ReportState)
              : (sort == "ReportState") ? travels.OrderByDescending(x => x.ReportState)
              : (direction == "descending" && sort == "LastName") ? travels.OrderByDescending(x => x.Person.LastName)
              : (sort == "ascending") ? travels.OrderByDescending(x => x.Person.LastName)
              : (direction == "descending") ? travels.OrderByDescending(x => x.StartDate)
              : travels.OrderBy(x => x.StartDate);

            
            
            
            DateTime fs;
            DateTime fe;
            if (!DateTime.TryParse(fStart, out fs)) fs = DateTime.MinValue;
            if (!DateTime.TryParse(fEnd, out fe)) fe = DateTime.MaxValue;


            if (Request.IsAuthenticated)
            {
                if (Person.GetByEmail(User.Identity.Name) == null)
                {
                    return RedirectToAction("NoUser" , "People");
                }
                else if (Person.GetByEmail(User.Identity.Name).UserInRoles.Select(x => x.Role.RoleName).Contains("Ülemus"))
                {
                    var ddl = from n in sorted select n.PersonId;
                    ViewBag.No = ddl;
                    var nimed = (from e in db.Travels select e).Take(no);

                    return View(sorted
                        .Where(x => x.Person.FirstName.ToUpper().Contains(otsing.ToUpper()) || x.Person.LastName.ToUpper().Contains(otsing.ToUpper()) || otsing == null || otsing == "")
                        .Where(x => x.StartDate <= fe && x.StartDate >= fs)
                        .AsEnumerable()
                        .Where(x =>
                                (x.Person.DepartmentID == Person.GetByEmail(User.Identity.Name).DepartmentID
                                && (x.Person.UserInRoles.Select(y => y.Role.RoleName).Contains("Töötaja") || x.Person.UserInRoles.Select(y => y.Role.RoleName).Contains("Raamatupidaja") || x.Person.UserInRoles.Select(y => y.Role.RoleName).Contains("Admin"))
                                && x.ReportState != 0 && x.ReportState != 2)
                                || (x.Person.Email == User.Identity.Name))
                        .ToPagedList(i ?? 1, 20));
                    
                }
                else if (Person.GetByEmail(User.Identity.Name).UserInRoles.Select(x => x.Role.RoleName).Contains("Direktor"))
                {
                    return View(sorted
                       .Where(x => x.Person.FirstName.ToUpper().Contains(otsing.ToUpper()) || x.Person.LastName.ToUpper().Contains(otsing.ToUpper()) || otsing == null || otsing == "")
                       .Where(x => x.StartDate <= fe && x.StartDate >= fs)
                       .AsEnumerable()
                       .Where(x =>
                                (x.Person.UserInRoles.Select(y => y.Role.RoleName).Contains("Direktor"))
                                || (x.Person.UserInRoles.Select(y => y.Role.RoleName).Contains("Ülemus") && x.ReportState != 0 && x.ReportState != 2))
                       .ToPagedList(i ?? 1, 20));
                }
                else if (Person.GetByEmail(User.Identity.Name).UserInRoles.Select(x => x.Role.RoleName).Contains("Raamatupidaja"))
                {
                    return View(sorted
                       .Where(x => x.Person.FirstName.ToUpper().Contains(otsing.ToUpper()) || x.Person.LastName.ToUpper().Contains(otsing.ToUpper()) || otsing == null || otsing == "")
                       .Where(x => x.StartDate <= fe && x.StartDate >= fs)
                       .AsEnumerable()
                       .Where(x =>
                                (x.Person.UserInRoles.Select(y => y.Role.RoleName).Contains("Raamatupidaja"))
                                || ((!x.Person.UserInRoles.Select(y => y.Role.RoleName).Contains("Raamatupidaja")) && x.ReportState == 3)
                                || ((!x.Person.UserInRoles.Select(y => y.Role.RoleName).Contains("Raamatupidaja")) && x.ReportState == 4))
                       .ToPagedList(i ?? 1, 20));
                }
                else if (Person.GetByEmail(User.Identity.Name).UserInRoles.Select(x => x.Role.RoleName).Contains("Töötaja")
                         || Person.GetByEmail(User.Identity.Name).UserInRoles.Select(x => x.Role.RoleName).Contains("Admin"))
                {
                    return View(sorted
                       .Where(x => x.Person.FirstName.ToUpper().Contains(otsing.ToUpper()) || x.Person.LastName.ToUpper().Contains(otsing.ToUpper()) || otsing == null || otsing == "")
                       .Where(x => x.StartDate <= fe && x.StartDate >= fs)
                       .AsEnumerable()
                       .Where(x => x.Person.FullName == Person.GetByEmail(User.Identity.Name).FullName)
                       .ToPagedList(i ?? 1, 20));
                }
                
            }

            return RedirectToAction("Login", "Account");

        }

  
        //Raamatupidajale Payout func
        public ActionResult Payout(int? id, Travel travel, ReportLog report)
        {
            Travel travels = db.Travels.Find(id);

            if (travels.Id == id && travels.ReportState == 3)
            {
                travels.ReportState = 4;
                db.SaveChanges();
                int ident = travel.Id;
                ReportLog.Log(report, $"Payout for travel: {ident}", Person.GetByEmail(User.Identity.Name).Id);
            }

            return RedirectToAction("Details", new { id = id });
        }
        //Raamatupidajale Reject func
        public ActionResult Reject(int? id, Travel travel, ReportLog report)
        {
            Travel travels = db.Travels.Find(id);

            if (travels.Id == id && travels.ReportState == 3)
            {
                travels.ReportState = 2;
                db.SaveChanges();
                int ident = travel.Id;
                ReportLog.Log(report, $"Travel claim rejected: {ident}", Person.GetByEmail(User.Identity.Name).Id);
            }

            return RedirectToAction("Index");
        }

        //Ülemusele accept func
        public ActionResult Accept(int? id, Travel travel, ReportLog report)
        {
            Travel travels = db.Travels.Find(id);

            if (travels.Id == id && travels.ReportState == 1)
            {
                travels.ReportState = 3;
                db.SaveChanges();
                int ident = travel.Id;
                ReportLog.Log(report, $"Travel claim accepted: {ident}", Person.GetByEmail(User.Identity.Name).Id);
            }

            return RedirectToAction("Details", new { id = id });
        }
        //Ülemusele Reject func
        public ActionResult Reject2(int? id, Travel travel, ReportLog report)
        {
            Travel travels = db.Travels.Find(id);

            if (travels.Id == id && travels.ReportState == 1)
            {
                travels.ReportState = 2;
                db.SaveChanges();
                int ident = travel.Id;
                ReportLog.Log(report, $"Travel claim rejected: {ident}", Person.GetByEmail(User.Identity.Name).Id);
            }

            return RedirectToAction("Index");
        }



        // GET: Travels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index", "Travels");
            }
            Travel travel = db.Travels.Find(id);
            if (travel == null)
            {
                return RedirectToAction("Index", "Travels");
            }

            decimal? sum = 0;

            foreach (var item in db.Spendings)
            {
                if (item.TravelId == id && item.Amount != null)
                {
                    sum += item.Amount;
                };
            }

            ViewBag.Sum = sum;

            return View(travel);


            //switch (submit)
            //{
            //    case "Väljamakse":
            //        travel.ReportState = 4;
            //        db.SaveChanges();
            //        return RedirectToAction("Index");

            //    case "Lükka tagasi":
            //        travel.ReportState = 2;
            //        db.SaveChanges();
            //        return RedirectToAction("Index");
            //}

        }

        // GET: Travels/Create
        public ActionResult Create()
        {
            Person person = Person.GetByEmail(User.Identity.Name);


            ViewBag.PersonId = new SelectList(db.People, "Id", "PersonId", person.Id);
            ViewBag.ReportState = new SelectList(Travel.States, "key", "Value");
            ViewBag.SpendingTypes = new SelectList(db.SpendingTypes, "Id", "SpendingName");


            return View(new Travel { PersonId = person.Id });
        }

        //return View(new Travel { PersonId = person.Id });



        // POST: Travels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,TravelName,StartDate,EndDate,Description,ReportState,PersonId")] Travel travel, string submit, ReportLog report)
        {
            if (ModelState.IsValid)
            {
                db.Travels.Add(travel);


                switch (submit)
                {
                    case "Salvesta":
                        db.SaveChanges();
                        int id1 = travel.Id;
                        ReportLog.Log(report, $"Created new travel: {id1}", Person.GetByEmail(User.Identity.Name).Id);
                        return RedirectToAction("Index");
                        break;

                    case "Lisa uus kulu":
                        Spending päevarahad = new Spending();
                        päevarahad.TravelId = travel.Id;
                        päevarahad.TypeId = 3;
                        päevarahad.Amount = travel.Päevarahad;
                        db.Spendings.Add(päevarahad);
                        db.SaveChanges();
                        int id2 = travel.Id;
                        ReportLog.Log(report, $"Created new travel: {id2}", Person.GetByEmail(User.Identity.Name).Id);
                        return RedirectToAction("Create", "Spendings", new { travelID = travel.Id });

                }

            }

            ViewBag.PersonId = new SelectList(db.People, "Id", "PersonID", travel.PersonId);
            return View(travel);
        }


        // GET: Travels/Edit/5
        public ActionResult Edit(int? id)
        {

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Travel travel = db.Travels.Find(id);

            if (travel == null)
            {
                return RedirectToAction("Index");
            }

            decimal? sum = 0;

            foreach (var item in db.Spendings)
            {
                if (item.TravelId == id && item.Amount != null)
                {
                    sum += item.Amount;
                };
            }

            ViewBag.Sum = sum;
            ViewBag.PersonId = new SelectList(db.People, "Id", "PersonID", travel.PersonId);
            ViewBag.ReportState = new SelectList(Travel.States, "key", "Value");

            return View(travel);

        }

        // POST: Travels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,TravelName,StartDate,EndDate,Description,ReportState,PersonId")] Travel travel, string submit, int? id, ReportLog report)
        {
            if (ModelState.IsValid)
            {
                //db.Travels.Add(travel);
                //Travel travels = db.Travels.Find(id);

                switch (submit)
                {
                    case "Salvesta":
                        db.Entry(travel).State = EntityState.Modified;
                        db.SaveChanges();
                        int id1 = travel.Id;
                        ReportLog.Log(report, $"Travel edited and saved: {id1}", Person.GetByEmail(User.Identity.Name).Id);
                        return RedirectToAction("Index");

                    case "Lisa uus kulu":
                        return RedirectToAction("Create", "Spendings", new { travelID = travel.Id });

                    case "Saada":
                        if (Person.GetByEmail(User.Identity.Name).UserInRoles.Select(x => x.Role.RoleName).Contains("Direktor"))
                        {
                            db.Entry(travel).State = EntityState.Modified;
                            travel.ReportState = 3;
                            db.SaveChanges();
                            int id2 = travel.Id;
                            ReportLog.Log(report, $"Travel claim sent to accounting: {id2}", Person.GetByEmail(User.Identity.Name).Id);
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            db.Entry(travel).State = EntityState.Modified;
                            travel.ReportState = 1;
                            db.SaveChanges();
                            int id3 = travel.Id;
                            ReportLog.Log(report, $"Travel claim sent to confirmation: {id3}", Person.GetByEmail(User.Identity.Name).Id);
                            return RedirectToAction("Index");
                        }

                }

            }

            ViewBag.PersonId = new SelectList(db.People, "Id", "PersonID", travel.PersonId);
            return View(travel);
        }

        // GET: Travels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Travel travel = db.Travels.Find(id);
            if (travel == null)
            {
                return HttpNotFound();
            }
            return View(travel);
        }

        // POST: Travels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, Travel travels, ReportLog report)
        {
            Travel travel = db.Travels.Find(id);
            db.Spendings.RemoveRange(db.Spendings.Where(x => x.TravelId == id));
            db.SaveChanges();
            db.Travels.Remove(travel);
            db.SaveChanges();
            int ident = travels.Id;
            ReportLog.Log(report, $"Travel claim deleted: {ident}", Person.GetByEmail(User.Identity.Name).Id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Arhiiv(string search, int? i, string otsing, string sort = "StartDate", string direction = "ascending", string fStart = "", string fEnd = "")
        {
            var travels = db.Travels.Include(t => t.Person);

            ViewBag.Sort = sort;
            ViewBag.Direction = direction;

            var sorted =
              (direction == "descending" && sort == "StartDate") ? travels.OrderByDescending(x => x.StartDate)
              : (sort == "StartDate") ? travels.OrderBy(x => x.StartDate)
              : (direction == "descending" && sort == "ReportState") ? travels.OrderByDescending(x => x.ReportState)
              : (sort == "ReportState") ? travels.OrderByDescending(x => x.ReportState)
              : (direction == "descending" && sort == "LastName") ? travels.OrderByDescending(x => x.Person.LastName)
              : (sort == "ascending") ? travels.OrderByDescending(x => x.Person.LastName)
              : (direction == "descending") ? travels.OrderByDescending(x => x.StartDate)
              : travels.OrderBy(x => x.StartDate);

            DateTime fs;
            DateTime fe;
            if (!DateTime.TryParse(fStart, out fs)) fs = DateTime.MinValue;
            if (!DateTime.TryParse(fEnd, out fe)) fe = DateTime.MaxValue;

            if (Request.IsAuthenticated)
            {

                if (Person.GetByEmail(User.Identity.Name).UserInRoles.Select(x => x.Role.RoleName).Contains("Ülemus"))
                {
                    return View(sorted
                       .Where(x => x.Person.FirstName.ToUpper().Contains(otsing.ToUpper()) || x.Person.LastName.ToUpper().Contains(otsing.ToUpper()) || otsing == null)
                       .Where(x => x.StartDate <= fe && x.StartDate >= fs)
                       .AsEnumerable()
                       .Where(x =>
                                (x.Person.DepartmentID == Person.GetByEmail(User.Identity.Name).DepartmentID
                                && x.Person.UserInRoles.Select(y => y.Role.RoleName).Contains("Töötaja")
                                && x.ReportState == 4)
                                || (x.Person.Email == User.Identity.Name) && x.ReportState == 4)
                       .ToPagedList(i ?? 1, 20));
                }
                else if (Person.GetByEmail(User.Identity.Name).UserInRoles.Select(x => x.Role.RoleName).Contains("Direktor"))
                {
                    return View(sorted
                       .Where(x => x.Person.FirstName.ToUpper().Contains(otsing.ToUpper()) || x.Person.LastName.ToUpper().Contains(otsing.ToUpper()) || otsing == null)
                       .Where(x => x.StartDate <= fe && x.StartDate >= fs)
                       .AsEnumerable()
                       .Where(x =>
                                (x.Person.UserInRoles.Select(y => y.Role.RoleName).Contains("Direktor")) && x.ReportState == 4
                                || (x.Person.UserInRoles.Select(y => y.Role.RoleName).Contains("Ülemus") && x.ReportState == 4))
                       .ToPagedList(i ?? 1, 20));
                }
                else if (Person.GetByEmail(User.Identity.Name).UserInRoles.Select(x => x.Role.RoleName).Contains("Raamatupidaja"))
                {
                    return View(sorted
                       .Where(x => x.Person.FirstName.ToUpper().Contains(otsing.ToUpper()) || x.Person.LastName.ToUpper().Contains(otsing.ToUpper()) || otsing == null)
                       .Where(x => x.StartDate <= fe && x.StartDate >= fs)
                       .AsEnumerable()
                       .Where(x =>
                                (x.Person.UserInRoles.Select(y => y.Role.RoleName).Contains("Raamatupidaja")) && x.ReportState == 4)

                       .ToPagedList(i ?? 1, 20));
                }
                else if (Person.GetByEmail(User.Identity.Name).UserInRoles.Select(x => x.Role.RoleName).Contains("Töötaja")
                         || Person.GetByEmail(User.Identity.Name).UserInRoles.Select(x => x.Role.RoleName).Contains("Admin"))
                {
                    return View(sorted
                       .Where(x => x.Person.FirstName.ToUpper().Contains(otsing.ToUpper()) || x.Person.LastName.ToUpper().Contains(otsing.ToUpper()) || otsing == null)
                       .Where(x => x.StartDate <= fe && x.StartDate >= fs)
                       .AsEnumerable()
                       .Where(x => x.Person.FullName == Person.GetByEmail(User.Identity.Name).FullName && x.ReportState == 4)
                       .ToPagedList(i ?? 1, 20));
                }

            }

            return RedirectToAction("Login", "Account");

        }
       
    }       
    
}
