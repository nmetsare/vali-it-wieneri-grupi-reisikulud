﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ReisikuludAutentimisega.Models;
using System.Linq.Expressions;
using PagedList.Mvc;
using PagedList;

namespace ReisikuludAutentimisega.Models
{
    partial class ReportLog : Controller
    {
        public static WienerEntities2 reportdb = new WienerEntities2();

        public static ReportLog Log(ReportLog report, string record, int personId)
        {
            ReportLog reportLogs = reportdb.ReportLogs.Add(report);

            reportLogs.LogDate = DateTime.Now;
            reportLogs.UserID = personId;
            reportLogs.LogRecord = record;
            reportdb.SaveChanges();

            return null;
        }


    }
}