﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ReisikuludAutentimisega.Models;
using PagedList.Mvc;
using PagedList;

namespace ReisikuludAutentimisega.Models
{
    partial class Person
    {
        static WienerEntities1 db = new WienerEntities1();

        public static string GetPersonID(string email)
            => GetByEmail(email)?.PersonID ?? "";


        public static Person GetByEmail(string email)
            => db.People.Where(x => x.Email == email).SingleOrDefault();

        public static Dictionary<int, string> States = new Dictionary<int, string>
        {
            {0, "Aktiveerimata" },
            {1, "Aktiivne" },
            {2, "Mitteaktiivne" },
            {3, "Lõpetatud" }
        };

        public string State => States[this.PersonState];

        public string FullName => LastName + "  " + FirstName;

        public bool IsInRole(string rolename)
            => this.UserInRoles
            .Select(x => x.Role.RoleName.ToLower()).ToList()
            .Intersect(rolename.Split(',').Select(y => y.Trim().ToLower())).Count() > 0;

        
           
            
    }
}
namespace ReisikuludAutentimisega.Controllers
{
    public class PeopleController : Controller
    {
        private WienerEntities1 db = new WienerEntities1();

        // GET: People
        public ActionResult Index(string search, int? i, string searchBy, string otsing)
        {
            var people = db.People.Include(p => p.Department).Include(p => p.UserInRoles);

            if (searchBy == "Nimi")
            {
                return View(people.Where(x => x.FirstName.ToUpper().Contains(otsing.ToUpper()) ||
                                              x.LastName.ToUpper().Contains(otsing.ToUpper()) ||
                                              otsing == null || otsing == "").ToList()
                                              .ToPagedList(i ?? 1, 20));
            }
            else
            {
                return View(people.Where(x => x.PersonID == otsing || otsing == null || otsing == "").ToList().ToPagedList(i ?? 1, 20));
            }
        }

        // GET: People/Details/5
        public ActionResult Details(int? id)
        {
            var people = db.People.Include(p => p.Department).Include(p => p.UserInRoles);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // GET: People/Create
        public ActionResult Create()
        {
            ViewBag.DepartmentID = new SelectList(db.Departments, "Id", "DepartmentName");
            ViewBag.Picture = new SelectList(db.Files, "Id", "Filename");
            ViewBag.PersonState = new SelectList(Person.States, "Key", "Value");
            return View();

        }

        // POST: People/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PersonID,FirstName,LastName,StartDate,PersonState,DepartmentID,Picture,Email")] Person person, string submit, ReportLog report)
        {
            if (ModelState.IsValid)
            {
                
                db.People.Add(person);
                db.SaveChanges();
                string id2 = person.FullName;
                ReportLog.Log(report, $"User created: {id2}", Person.GetByEmail(User.Identity.Name).Id);
                return RedirectToAction("Create", "UserInRoles", new { personID = person.Id });               


            }

            ViewBag.DepartmentID = new SelectList(db.Departments, "Id", "DepartmentName", person.DepartmentID);
            ViewBag.Picture = new SelectList(db.Files, "Id", "Filename", person.Picture);
            ViewBag.PersonState = new SelectList(Person.States, "Key", "Value", person.PersonState);
            return View(person);
        }

        // GET: People/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index", "People");
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return RedirectToAction("Index", "People");
            }
            ViewBag.DepartmentID = new SelectList(db.Departments, "Id", "DepartmentName", person.DepartmentID);
            ViewBag.Picture = new SelectList(db.Files, "Id", "Filename", person.Picture);
            ViewBag.PersonState = new SelectList(Person.States, "Key", "Value", person.PersonState);
            return View(person);
        }

        // POST: People/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PersonID,FirstName,LastName,StartDate,PersonState,DepartmentID,Picture,Email")] Person person, string submit, ReportLog report)
        {
            
            if (ModelState.IsValid)
            {
                
                switch (submit)
                {
                    case "Salvesta":
                        db.Entry(person).State = EntityState.Modified;
                        db.SaveChanges();
                        string id1 = person.FullName;
                        ReportLog.Log(report, $"User edited: {id1}", Person.GetByEmail(User.Identity.Name).Id);
                        return RedirectToAction("Index");
                        break;

                    case "Lisa roll":
                        return RedirectToAction("Create", "UserInRoles", new { personID = person.Id });
                }


            }
           
            ViewBag.DepartmentID = new SelectList(db.Departments, "Id", "DepartmentName", person.DepartmentID);
            ViewBag.PersonID = new SelectList(db.People, "Id", "PersonID", person.Id);
            ViewBag.UserInRolesID = new SelectList(db.UserInRoles, "Id", "PersonID", person.Id);
            ViewBag.PersonState = new SelectList(Person.States, "Key", "Value", person.PersonState);
            return View(person);
        }
        // GET: People/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id,ReportLog report)
        {
            Person person = db.People.Find(id);
            db.UserInRoles.RemoveRange(db.UserInRoles.Where(x => x.PersonId == id));
            db.SaveChanges();
            db.People.Remove(person);
            db.SaveChanges();
            string id1 = person.FullName;
            ReportLog.Log(report, $"User deleted: {id1}", Person.GetByEmail(User.Identity.Name).Id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult NoUser(int? id)
        {
            return View();
        }

    }
}
