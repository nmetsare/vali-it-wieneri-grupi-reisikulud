﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ReisikuludAutentimisega.Models;

namespace ReisikuludAutentimisega.Controllers
{
    public class UserInRolesController : Controller
    {
        private WienerEntities1 db = new WienerEntities1();

        // GET: UserInRoles
        public ActionResult Index()
        {
            var userInRoles = db.UserInRoles.Include(u => u.Person).Include(u => u.Role);
            return View(userInRoles.ToList());
        }

        // GET: UserInRoles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserInRole userInRole = db.UserInRoles.Find(id);
            if (userInRole == null)
            {
                return HttpNotFound();
            }
            return View(userInRole);
        }

        // GET: UserInRoles/Create
        public ActionResult Create(int PersonID)
        {
            ViewBag.PersonId = new SelectList(db.People, "Id", "PersonID");
            ViewBag.RoleId = new SelectList(db.Roles, "Id", "RoleName");
            return View();
        }

        // POST: UserInRoles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,PersonId,RoleId")] UserInRole userInRole, ReportLog report)
        {
            if (ModelState.IsValid)
            {
                db.UserInRoles.Add(userInRole);
                db.SaveChanges();
                int id1 = userInRole.RoleId;
                int id2 = userInRole.PersonId;
                ReportLog.Log(report, $"Role \"{id1}\" added to user {id2}", Person.GetByEmail(User.Identity.Name).Id);
                return RedirectToAction("Edit", "People", new { id = userInRole.PersonId });
            }

            ViewBag.PersonId = new SelectList(db.People, "Id", "PersonID", userInRole.PersonId);
            ViewBag.RoleId = new SelectList(db.Roles, "Id", "RoleName", userInRole.RoleId);
            return View(userInRole);
        }

        // GET: UserInRoles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            UserInRole userInRole = db.UserInRoles.Find(id);
            if (userInRole == null)
            {
                return RedirectToAction("Index");
            }
            ViewBag.PersonId = new SelectList(db.People, "Id", "PersonID", userInRole.PersonId);
            ViewBag.RoleId = new SelectList(db.Roles, "Id", "RoleName", userInRole.RoleId);
            return View(userInRole);
            
            
        }

        // POST: UserInRoles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,PersonId,RoleId")] UserInRole userInRole, string submit, ReportLog report)
        {
            if (ModelState.IsValid)
            {
                
                db.Entry(userInRole).State = EntityState.Modified;
                db.SaveChanges();
                int id1 = userInRole.RoleId;
                int id2 = userInRole.PersonId;
                ReportLog.Log(report, $"Role \"{id1}\" added to user {id2}", Person.GetByEmail(User.Identity.Name).Id);
                return RedirectToAction("Index");
                                          
                
            }
            ViewBag.PersonId = new SelectList(db.People, "Id", "PersonID", userInRole.PersonId);
            ViewBag.RoleId = new SelectList(db.Roles, "Id", "RoleName", userInRole.RoleId);
            return View(userInRole);
        }
        //public ActionResult Edit([Bind(Include = "id,PersonId,RoleId")] UserInRole userInRole, string submit)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.UserInRoles.Add(userInRole);

        //        switch (submit)
        //        {
        //            case "Save":
        //                db.Entry(userInRole).State = EntityState.Modified;
        //                db.SaveChanges();
        //                return RedirectToAction("Index");

        //            //case "Muuda rolli":
        //            //    return RedirectToAction("Edit", "Edit", new { PersonID = userInRole.PersonId });

        //        }

        //    }

        //    ViewBag.PersonId = new SelectList(db.People, "Id", "PersonID", userInRole.PersonId);
        //    ViewBag.RoleId = new SelectList(db.Roles, "Id", "RoleName", userInRole.RoleId);
        //    return View(userInRole);
        //}
        // GET: UserInRoles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserInRole userInRole = db.UserInRoles.Find(id);
            if (userInRole == null)
            {
                return HttpNotFound();
            }
            return View(userInRole);
        }

        // POST: UserInRoles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, ReportLog report)
        {
            UserInRole userInRole = db.UserInRoles.Find(id);
            db.UserInRoles.Remove(userInRole);
            db.SaveChanges();
            int id1 = userInRole.RoleId;
            int id2 = userInRole.PersonId;
            ReportLog.Log(report, $"Role \"{id1}\" removed from user {id2}", Person.GetByEmail(User.Identity.Name).Id);
            return RedirectToAction("Edit", "People", new { id = userInRole.PersonId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
